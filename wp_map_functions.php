<?php
add_action('admin_menu' , function(){
	Wp_map_settings::add_menu_page();
});

add_action('admin_init' , function(){
	new Wp_map_settings();
});

Wp_map_settings::add_shortcodes();

function wp_map_register_widgets() {
	register_widget( 'Wp_map_wisgets' );
}

add_action( 'widgets_init', 'wp_map_register_widgets' );

function wp_map_get_post_template(){
	$templates = new Wp_Map_Template_Loader;
	ob_start();
	$templates->get_template_part( 'post', 'template' );
	return ob_get_clean();
}

function wp_map_get_infowindow_template(){
	$templates = new Wp_Map_Template_Loader;
	ob_start();
	$templates->get_template_part( 'infowindow', 'template' );
	return ob_get_clean();
}
add_filter('widget_text', 'do_shortcode');

