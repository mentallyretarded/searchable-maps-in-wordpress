(function($) {
    "use strict";
	$('#ms-map').mapSearch({
		initialPosition:[wp_map_data.init_lat, wp_map_data.init_lng],
		request_uri : wp_map_data.request_uri,
		search_box : true,
		icon : wp_map_data.icon,
		highlighted_icon : wp_map_data.highlighted_icon,
		listing_latlng: function(listing){
				return [listing.latitude , listing.longitude];
			},
		listing_template : function(listing){	
			return listing.content;
			},
		infowindow_content : function(listing){	
					return listing.infowindow ;
			},
		filters_form : '#filters',
	});
}(jQuery));